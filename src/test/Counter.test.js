import React from 'react';
import ReactDOM from 'react-dom';
import Counter from '../container/Counter';
import configureStore from 'redux-mock-store'
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import reducer from '../reducer/reducer'
import {INCREMENT,DECREMENT,RESET} from '../actions/actionTypes'
configure({ adapter: new Adapter() });

const initialState = {};
const mockStore = configureStore();

test('renders without crashing', () => {
  const store = mockStore(initialState);
  const div = document.createElement('div');
  ReactDOM.render(<Counter store={store} />, div);
  ReactDOM.unmountComponentAtNode(div);
});

test('counter increase when plus button clicked', () => {
  const store = mockStore(initialState);
  let wrapper = mount(<Counter store={store} />)
  wrapper.find('.increment').simulate('click')
  const action = store.getActions()
  expect(action[0].type).toBe(INCREMENT)
})

test('counter decrement when minus button clicked', () => {
  const store = mockStore(initialState);
  let wrapper = mount(<Counter store={store} />)
  wrapper.find('.decrement').simulate('click')
  const action = store.getActions()
  expect(action[0].type).toBe(DECREMENT)
})

test('counter will be zero when reset button clicked', () => {
  const store = mockStore(initialState);
  let wrapper = mount(<Counter store={store} />)
  wrapper.find('.reset-button').simulate('click')
  const action = store.getActions()
  expect(action[0].type).toBe(RESET)
})

test('should work INCREMENT, DECREMENT and RESET reducer correctly', () => {
  expect(reducer({count:10}, { type: INCREMENT })).toEqual({ count: 11})
  expect(reducer({count:10}, { type: DECREMENT})).toEqual({ count: 9})
  expect(reducer({count:10}, { type: RESET })).toEqual({ count: 0})
})